# lcd's baseline controller...
# like rbc.py

# exposed functions:
#	One timers: start(), end(), 
# 	Usage     : flushFrame()

import log, globe, serial
from lcd_frames import *

# if (globe.FAKE):
# 	from test import fakeRBC as serial
# else:
# 	import serial

PORT = ""			# This is like /dev/ttyUSBx, where X is an integer
CIN_TIMEOUT = 1

Conn = None

def begin(fake_handle = None):
	"""Kind of constructor for this module. Core scan """
	global PORT, Conn, CIN_TIMEOUT
	PORT = globe.ports[globe.LCD]
	
	args = {'port': str(PORT), 'baudrate': 9600, 'timeout':CIN_TIMEOUT}
	# if (globe.FAKE):
	# 	args['handle'] = fake_handle

	try:
		Conn = serial.Serial(**args)
		log.Info("Lcd connected on port: " + PORT)
	except:
		log.pr("LCD Failed to conn, port name: " + str(PORT))
		return False
	return True


def end():
	"""stops rbc conn"""
	global Conn
	Conn.close()


def flushFrame(fr):
	global Conn
	msg = fr.getCmd()	# the full frame rendering command
	# log.Info("on lcd " + msg)
	if (Conn == None):
		log.Error("LCD not present")
		return
	Conn.write(msg)


