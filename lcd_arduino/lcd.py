import initDevices, globe, log, serial, time, math, pprint

SIG = "LCD" 
HIGHLIGHTED = None
CURSOR_AT = None
CIN_TIMEOUT = 1
PORT = None
Conn = None
delim = '%'
commDelim = '~'
text_space = '%'

Rectangles_List = [None]*10
class Rectangle():
	def __init__(self, x, y, wrect_idth, height, rect_id):
		self.x = x; self.y = y; self.wrect_idth = wrect_idth; self.height = height; self.rect_id = rect_id; self.num_rows = 0;
		self.chars_inrow = 0; self.scroll_count = 0; self.set_text_count = 0; self.text_list = [];


# Kind of constructor for this module.
def initLcd():
	global PORT, Conn, CIN_TIMEOUT
	PORT = globe.ports[globe.LCD]
	try:
		Conn = serial.Serial(str(PORT), 9600, timeout = CIN_TIMEOUT)
		Conn.read(3)
		# Conn.close() 
	except:
		log.pr("LCD Failed to conn, port name: " + str(PORT))
		return False
	return True

# Check if lcd is connected to the raspberry pi device

def isConnected():
	if (globe.ports[globe.LCD] is not None):
		return True
	return False

def return_ansid(rect_idplusdelim):
	return int(rect_idplusdelim.replace('%', ' '))
	#return int(rect_idplusdelim)

# Return-type string
def commToSend(comm):
	global Conn
	# Conn.open()
	print "comm = "+comm
	Conn.read(4)	# just a hack need to fix this. read all previous buffer
	time.sleep(0.5)
	Conn.write(comm)
	incoming = Conn.read(2)
	print "return = "+ incoming
	# Conn.close()
	return incoming.strip()

def getRect(x, y, wrect_idth, height):
	if (not (isinstance(x, int) and isinstance(y, int) and isinstance(wrect_idth, int) and isinstance(height, int))):
		return -1;
	if (wrect_idth < 0 or height < 0):
		return -2;
	comm = 'getRect' + delim + `x` + delim + `y` + delim + `wrect_idth` + delim + `height`+commDelim
	rect_id = commToSend(comm)
	int_rect_id = return_ansid(rect_id)
	Rectangles_List[int_rect_id] = Rectangle(x, y, wrect_idth, height, int_rect_id)
	return int_rect_id

def drawHLine(x, y, wrect_idth):
	if (not (isinstance(x, int) and isinstance(y, int) and isinstance(wrect_idth, int))):
		return -1;
	if (wrect_idth < 0 or x < 0 or y < 0):
		return -2;
	comm = 'drawHLine' + delim + `x` + delim + `y` + delim + `wrect_idth`+commDelim
	ans_rect_id = commToSend(comm)
	return return_ansid(ans_rect_id)

def drawVLine(x, y, height):
	if (not (isinstance(x, int) and isinstance(y, int) and isinstance(height, int))):
		return -1;
	if (height < 0 or x < 0 or y < 0):
		return -2;
	comm = 'drawVLine' + delim + `x` + delim + `y` + delim + `height`+commDelim
	ans_rect_id = commToSend(comm)
	return return_ansid(ans_rect_id)

def compute_text(text, rect_id):
	lines_list = text.split("\n")
	Rect = Rectangles_List[rect_id]
	print(Rect.text_list)
	print("Rectangle Id "+ str(rect_id))
	print(text)
	print(lines_list)
	count_characters = int(math.floor(Rect.wrect_idth/6))
	total_lines = int(math.floor(Rect.height/8))
	Rect.num_rows = total_lines
	Rect.chars_inrow = count_characters
	for i in range(len(lines_list)):
		current_string = lines_list[i]
		if(len(lines_list[i]) > count_characters):
			for j in range(0, len(lines_list[i]), count_characters):
				if((j+count_characters) > len(lines_list[i])):
					Rect.text_list.append(current_string[j:])
				else:
					Rect.text_list.append(current_string[j:j+count_characters])
		else:
			Rect.text_list.append(current_string)
	if(len(Rect.text_list) > total_lines):
		new_text = ""
		for i in range(total_lines):
			new_text = new_text + Rect.text_list[i] + '\n'
	else:
		new_text = text
	print(Rect.text_list)
	return new_text

def scroll_down(rect_id):
	if (not (isinstance(rect_id, int))):
		return -1;
	Rect = Rectangles_List[rect_id]
	if(Rect.scroll_count >= len(Rect.text_list) - Rect.num_rows):
		print("No Down Scroll Possible !!!")
	else:
		Rect.scroll_count = Rect.scroll_count + 1
		rec_text = ""
		for i in range(Rect.scroll_count, Rect.scroll_count + Rect.num_rows):
			rec_text = rec_text + Rect.text_list[i] + '\n'
		setText(rect_id, rec_text, 0, 1)

def scroll_up(rect_id):
	if (not (isinstance(rect_id, int))):
		return -1;
	Rect = Rectangles_List[rect_id]
	if(Rect.scroll_count <= 0):
		print("No Up Scroll Possible !!!")
	else:
		Rect.scroll_count = Rect.scroll_count - 1
		rec_text = ""
		for i in range(Rect.scroll_count, Rect.scroll_count + Rect.num_rows):
			rec_text = rec_text + Rect.text_list[i] + '\n'
		setText(rect_id, rec_text, 0, 1)

def highLightRect(x, y, wrect_idth, height):
	if (not (isinstance(x, int) and isinstance(y, int) and isinstance(height, int) and isinstance(wrect_idth, int))):
		return -1;
	if (wrect_idth < 0 or height < 0 or x < 0 or y < 0):
		return -2;
	comm = 'highLightRect' + delim + `x` + delim + `y` + delim + `wrect_idth` + delim + `height`+commDelim
	ans_rect_id = commToSend(comm)
	return return_ansid(ans_rect_id)

#highl shows wheter to highlight or not this particular text

def setText(rect_id, text, append_option, highl):
	if (not (isinstance(append_option, int) and (isinstance(rect_id, int) and isinstance (highl, int)))):
		return -1;
	if(append_option == 0):
		clearRect(rect_id)
	print("This is text !!! " + text)
	Rect = Rectangles_List[rect_id]
	new_text = text
	if(Rect.set_text_count == 0):
		new_text = compute_text(text, rect_id)
	comm = 'setText' + delim + `rect_id` + delim + new_text + delim + `highl` +commDelim
	Rect.set_text_count = Rect.set_text_count + 1
	status = commToSend(comm)
	status_rect_id = return_ansid(status)
	return status_rect_id

def clearRect(rect_id):
	if (not isinstance(rect_id, int)):
		return -1;
	comm = 'clearRect' + delim + `rect_id`+commDelim
	status = commToSend(comm)
	return return_ansid(status)

def freeRect(rect_id):
	if (not isinstance(rect_id, int)):
		return -1;
	comm = 'freeRect' + delim + `rect_id`+commDelim
	status = commToSend(comm)
	return return_ansid(status)

def getHeight():
	comm = 'getHeight'+commDelim
	height = commToSend(comm)
	return return_ansid(height)

def getWrect_idth():
	comm = 'getWrect_idth'+commDelim
	wrect_idth = commToSend(comm)
	return return_ansid(wrect_idth)

def stringWrect_idth(str):
	comm = 'stringWrect_idth' + delim + str +commDelim
	wrect_idth = commToSend(comm)
	return return_ansid(wrect_idth)

# Returns screen state by returning a list of all the current screen elements present with the system-

# def changeElement(newScreenElement):
# 	if not 'row' in newScreenElement:
# 		raise Exception('Row Number mssing in screen element') 
# 	if not (isinstance(newScreenElement['row'], int) and newScreenElement['row'] < 50):
# 		raise Exception('Row Number should be integer value < 50') 
# 	if not 'horizontalAlignment' in newScreenElement:
# 		newScreenElement['horizontalAlignment']='LEFT'
# 	if not 'text' in newScreenElement:
# 		newScreenElement['text']=''
# 	if not 'fontSize' in newScreenElement:
# 		newScreenElement['fontSize']=DEFAULT_FONT_SIZE
# 	if not 'type' in newScreenElement:
# 		newScreenElement['type']='LIST'
# 	if not 'isHighLighted' in newScreenElement:
# 		newScreenElement['isHighLighted']=False

# 	SCREEN_ELEMENTS[newScreenElement['row']] = newScreenElement
# 	System.changeScreen()


# # def refreshElement(row):
# # 	System.changeScreen(newScreenElement)

# def cursorAt(row):
# 	System.CURSOR_AT=row
# 	System.changeScreen()

# # Get element with particular rect_id from screen elements

# def getElementByRow(row):
# 	for ScreenElement in SCREEN_ELEMENTS:
# 		if ScreenElement['row']==row:
# 			return ScreenElement
# 	return 


if __name__ == "__main__":
	initDevices.begin()
	initLcd()
	rect_id = getRect(0, 0, 20, 20)
	rect_id_1 = getRect(40, 40, 60, 60)
	setText(rect_id, 'Narendra Kumar was a \ngood man\n who can throw dirt on somebody and   make them unhappy', 1, 1)
	print(rect_id)
	setText(rect_id_1, 'Ankit Aggarwal is a \nbad body\nappy used to go to scholl with nice sho\n ilod  fefevf fdfwe', 1, 1)
	print(rect_id_1)
	for i in range(5):
		time.sleep(1)
		scroll_down(rect_id)
		scroll_down(rect_id_1)
	#drawHLine(0, 15, 128)
	#drawVLine(30, 0, 15)
	#print(getHeight());
	#print(getWrect_idth());


def test():
	initDevices.begin()
	initLcd()
	rect_id = getRect(2+6, 15, 25, 25)
	rect_id_1 = getRect(2, 40+2, 37, 18)
	setText(rect_id, '||||||||||||||||||||||||||', 1, 1)
	setText(rect_id_1, ' Setup\n user', 1, 1)

	rect = getRect(0, 0, 128, 9)
	setText(rect, '........................................', 1, 1)

	rect_id = getRect(46+6, 15, 25, 25)
	rect_id_1 = getRect(46, 40+2, 37, 18)
	setText(rect_id, '||||||||||||', 1, 1)
	setText(rect_id_1, ' Setup\n user', 1, 1)

	rect_id = getRect(90+6, 15, 25, 25)
	rect_id_1 = getRect(90, 40+2, 37, 18)
	setText(rect_id, '||||||||||||', 1, 1)
	setText(rect_id_1, 'Setup\n  user', 1, 1)

	# for i in range(5):
	# 	time.sleep(1)
	# 	scroll_down(rect_id)
	# 	scroll_down(rect_id_1)
