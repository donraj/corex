/*
 * GLCD for lcd-coreX
 *
 */

#include <glcd.h>
#include "fonts/allFonts.h"         // system and arial14 fonts are used

#define SIG "LCD"
#define MAX_RECT 10
#define MAX_ARGS 10
#define delim '%'
#define commDelim '~'

gText rect[MAX_RECT];
boolean rectUsed[MAX_RECT];
String comm[MAX_ARGS];
int currArg=0;

void setup() {

	GLCD.Init(NON_INVERTED);   // initialise the library, non inverted writes pixels onto a clear screen
	GLCD.ClearScreen();  
	GLCD.SelectFont(System5x7, BLACK); // font for the default text area

	for (int i=0; i < MAX_RECT; i++)
		rectUsed[i] = false;

	resetComm();
	
	Serial.begin(9600);
	while(!Serial) {};
	Serial.print(SIG);
	Serial.print(delim);

}

void resetComm() {
	currArg = 0;
	for (int i=0; i < MAX_ARGS; i++)
		comm[i] = "";
}

void  loop() {  
	int flag=0;

	while (Serial.available() > 0) {
		char incomingChar = Serial.read();

		if (incomingChar == commDelim) {
			flag=1;
			break;
		}

		if (incomingChar == delim) {
			if(currArg == MAX_ARGS - 1) {
				flag = 1;
				break;
			}
			currArg++;
			continue;
		}
		comm[currArg] += incomingChar;
	}

	if (flag==1) {
		if (comm[0].equals("getRect")) {
			int id=createNewRect(comm[1].toInt(), comm[2].toInt(), comm[3].toInt(), comm[4].toInt());
			Serial.print(id);
			Serial.print(delim);
		}

		else if (comm[0].equals("setText")) {
			int strLen = comm[2].length() + 1; 
			char charArray[strLen];
			comm[2].toCharArray(charArray, strLen);
			int success=setTextInRect(comm[1].toInt(), charArray, comm[3].toInt());
			Serial.print(success);
			Serial.print(delim);
		}

		else if (comm[0].equals("clearRect")) {
			int success=clearRect(comm[1].toInt());
			Serial.print(success);
			Serial.print(delim);
		}

		else if (comm[0].equals("freeRect")) {
			int success=freeRect(comm[1].toInt());
			Serial.print(success);
			Serial.print(delim);
		}

		else if (comm[0].equals("getHeight")) {
			int height=getHeight();
			Serial.print(height);
			Serial.print(delim);
		}

		else if (comm[0].equals("getWidth")) {
			int width=getWidth();
			Serial.print(width);
			Serial.print(delim);
		}

		else if (comm[0].equals("stringWidth")) {
			int strLen = comm[2].length() + 1; 
			char charArray[strLen];
			comm[1].toCharArray(charArray, strLen);
			int width=stringWidth(charArray);
			Serial.print(width);
			Serial.print(delim);
		}

		else if (comm[0].equals("highLightRect")) {
			GLCD.InvertRect(comm[1].toInt(), comm[2].toInt(), comm[3].toInt(), comm[4].toInt());
			int return_id = 1;
			Serial.print(return_id);
			Serial.print(delim);
		}

		else if (comm[0].equals("drawLine")) {

		}

		else if (comm[0].equals("drawHLine")) {
			GLCD.DrawHLine(comm[1].toInt(), comm[2].toInt(), comm[3].toInt(), BLACK);
			int return_id = 1;
			Serial.print(return_id);
			Serial.print(delim);
		}

		else if (comm[0].equals("drawVLine")) {
			GLCD.DrawVLine(comm[1].toInt(), comm[2].toInt(), comm[3].toInt(), BLACK);
			int return_id = 1;
			Serial.print(return_id);
			Serial.print(delim);
		}
		
		resetComm();
	}

}

int getHeight() {
	return GLCD.Height;
}

int getWidth() {
	return GLCD.Width;
}

int createNewRect(int x, int y, int width, int height) {
	
	int i=0;
	for (;rectUsed[i] && (i<MAX_RECT);i++) {}
	
	if(i==MAX_RECT) {
		return -1;
	}
	
	// rect[i] = gText(x, y, width, height,Arial_14); 
	rect[i].DefineArea(x, y, x + width, y + height, SCROLL_UP);
	rect[i].SelectFont(System5x7, BLACK);
	//rect[i].SetTextMode(SCROLL_DOWN);
	rect[i].CursorTo(0,0);
	rectUsed[i]=true;

	return i; // Position inserted in rect array
}

int setTextInRect(int posInRectArray, char* text, int highl) {

	if (posInRectArray>=MAX_RECT || posInRectArray<0)
		return -1;

	if (!rectUsed[posInRectArray])
		return -2;
	if(highl == 1)
	{
		rect[posInRectArray].SetFontColor(WHITE);
		rect[posInRectArray].print(text);
	}
	else
	{	
		rect[posInRectArray].print(text);
	}
	return 1;
}

int clearRect(int posInRectArray) {
	if (posInRectArray>=MAX_RECT || posInRectArray<0)
		return -1;

	if (!rectUsed[posInRectArray])
		return -2;

	rect[posInRectArray].print("");
	return 1;
}

int freeRect(int posInRectArray) {
	if (posInRectArray>=MAX_RECT || posInRectArray<=0)
		return -1;

	if (!rectUsed[posInRectArray])
		return -2;

	rectUsed[posInRectArray]=false;
	return 1;
}

int stringWidth(char* text) {
	return GLCD.StringWidth(text);
}

//
//void introScreen(){  
//  GLCD.DrawBitmap(icon, 32,0); //draw the bitmap at the given x,y position
//  countdown(3);
//  GLCD.ClearScreen();
//  GLCD.SelectFont(Arial_14); // you can also make your own fonts, see playground for details   
//  GLCD.CursorToXY(GLCD.Width/2 - 44, 3);
//  GLCD.print("GLCD version ");
//  GLCD.print(GLCD_VERSION, DEC);
//  GLCD.DrawRoundRect(8,0,GLCD.Width-19,17, 5);  // rounded rectangle around text area   
//  countdown(3);  
//  GLCD.ClearScreen(); 
//  scribble(5000);  // run for 5 seconds
//  moveBall(6000); // kick ball for 6 seconds
//  GLCD.SelectFont(System5x7, BLACK);
//  showCharacters("5x7 font:", System5x7);
//  countdown(3);
//  showCharacters("Arial_14:", Arial_14);
//  countdown(3);
//  textAreaDemo();
//  scrollingDemo();
//}
//
//void showCharacters(char * title, Font_t font) {
//  // this displays the desired font
//  GLCD.ClearScreen();  
//  GLCD.CursorTo(0,0);
//  GLCD.print(title);
//  GLCD.DrawRoundRect(GLCD.CenterX + 2, 0, GLCD.CenterX -3, GLCD.Bottom, 5);  // rounded rectangle around text area 
//  textArea.DefineArea(GLCD.CenterX + 5, 3, GLCD.Right-2, GLCD.Bottom-4, SCROLL_UP); 
//  textArea.SelectFont(font, BLACK);
//  textArea.CursorTo(0,0);
//  for(char c = 32; c < 127; c++){
//    textArea.print(c);  
//    delay(20);
//  }
//}
//
//void drawSpinner(byte pos, byte x, byte y) {   
//  // this draws an object that appears to spin
//  switch(pos % 8) {
//    case 0 : GLCD.DrawLine( x, y-8, x, y+8);        break;
//    case 1 : GLCD.DrawLine( x+3, y-7, x-3, y+7);    break;
//    case 2 : GLCD.DrawLine( x+6, y-6, x-6, y+6);    break;
//    case 3 : GLCD.DrawLine( x+7, y-3, x-7, y+3);    break;
//    case 4 : GLCD.DrawLine( x+8, y, x-8, y);        break;
//    case 5 : GLCD.DrawLine( x+7, y+3, x-7, y-3);    break;
//    case 6 : GLCD.DrawLine( x+6, y+6, x-6, y-6);    break; 
//    case 7 : GLCD.DrawLine( x+3, y+7, x-3, y-7);    break;
//  } 
//}
//
//void  textAreaDemo()
//{ 
//  showArea( textAreaFULL,       "Full"); 
//  showArea( textAreaTOP,        "Top");
//  showArea( textAreaBOTTOM,     "Bottom"); 
//  showArea( textAreaRIGHT,      "Right");
//  showArea( textAreaLEFT,       "Left"); 
//  showArea( textAreaTOPLEFT,    "Top Left");
//  showArea( textAreaTOPRIGHT,   "Top Right");
//  showArea( textAreaBOTTOMLEFT, "Bot Left"); 
//  showArea( textAreaBOTTOMRIGHT,"Bot Right");  
//}
//
//void showArea(predefinedArea area, char *description)
//{
//  GLCD.ClearScreen(); 
//  GLCD.DrawBitmap(icon, 0,  0); 
//  GLCD.DrawBitmap(icon, 64, 0); 
//  textArea.DefineArea(area);
//  textArea.SelectFont(System5x7);
//  textArea.SetFontColor(WHITE); 
//  textArea.ClearArea(); 
//  textArea.println(description);
//  textArea.print("text area");
//  delay(1000);
//  textArea.SetFontColor(WHITE); 
//  textArea.ClearArea();
//  showLines(10);
//  delay(1000);   
//  textArea.ClearArea();
//  textArea.SetFontColor(BLACK);   
//  showLines(10);
//  delay(1000); 
//}
//
//void showAscii()
//{
//  for( char ch = 64; ch < 127; ch++)
//  {
//    GLCD.print(ch);
//    delay(theDelay);
//  }   
//}
//
//void showLines(int lines)
//{
//  for(int i = 1; i <= lines; i++)
//  { 
//    textArea << " Line  " << i << endl;  
//    delay(theDelay);  // brief pause between lines
//  }
//}
//
//
//void countdown(int count){
//  while(count--){  // do countdown  
//    countdownArea.ClearArea(); 
//    countdownArea.print(count);
//    delay(1000);  
//  }  
//}
//
//void scrollingDemo()
//{
//  GLCD.ClearScreen();  
//  textAreaArray[0].DefineArea( textAreaTOPLEFT);  
//  textAreaArray[0].SelectFont(System5x7, WHITE);
//  textAreaArray[0].CursorTo(0,0);
//  textAreaArray[1].DefineArea( textAreaTOPRIGHT, SCROLL_DOWN);  // reverse scroll
//  textAreaArray[1].SelectFont(System5x7, BLACK);
//  textAreaArray[1].CursorTo(0,0);
//  textAreaArray[2].DefineArea(textAreaBOTTOM); 
//
//  textAreaArray[2].SelectFont(Arial_14, BLACK);
//  textAreaArray[2].CursorTo(0,0);
//
//  for(byte area = 0; area < 3; area++)
//  {
//    for( char c = 64; c < 127; c++)
//      textAreaArray[area].print(c);
//    delay(theDelay);    
//  }
//  for(char c = 32; c < 127; c++)
//  {
//    for(byte area = 0; area < 3; area++)
//      textAreaArray[area].print(c);
//    delay(theDelay);
//  }  
//
//  for(byte area = 0; area< 3; area++)
//  {
//    textAreaArray[area].ClearArea();
//  }
//  for(int x = 0; x < 15; x++)
//  {
//    for(byte area = 0; area < 3; area++)
//    {
//      textAreaArray[area].print("line ");
//      textAreaArray[area].println(x);
//      delay(50);
//    }
//  } 
//  delay(1000);
//}
//
///*
// * scribble drawing routine adapted from TellyMate scribble Video sketch
// * http://www.batsocks.co.uk/downloads/tms_scribble_001.pde
// */
//void scribble( const unsigned int duration )
//{
//  const  float tick = 1/128.0;
//  float g_head_pos = 0.0;
//  
//  for(unsigned long start = millis();  millis() - start < duration; )
//  {
//    g_head_pos += tick ;
//
//    float head = g_head_pos ;
//    float tail = head - (256 * tick) ;
//
//    // set the pixels at the 'head' of the line...
//    byte x = fn_x( head ) ;
//    byte y = fn_y( head ) ;
//    GLCD.SetDot( x , y , BLACK) ;
//
//    // clear the pixel at the 'tail' of the line...
//    x = fn_x( tail ) ;
//    y = fn_y( tail ) ;  
//     GLCD.SetDot( x , y , WHITE) ;
//  }
//}
//
//byte fn_x( float tick )
//{
//  return (byte)(GLCD.Width/2 + (GLCD.Width/2-1) * sin( tick * 1.8 ) * cos( tick * 3.2 )) ;
//}
//
//byte fn_y( float tick )
//{
//  return (byte)(GLCD.Height/2 + (GLCD.Height/2 -1) * cos( tick * 1.2 ) * sin( tick * 3.1 )) ;
//}
//
//
//void moveBall(unsigned int duration) 
//{
//
//const byte ballR = 4;
//int ballX = GLCD.CenterX + 5;   // X position of the ball 
//int ballY = GLCD.CenterY;       // Y position of the ball
//int ballDirectionY = 1;        // X direction of the ball
//int ballDirectionX = 1;        // Y direction of the ball
//
//  GLCD.ClearScreen();
//  for(unsigned long start = millis();  millis() - start < duration; )
//  {
//    // if ball goes off the screen top or bottom, reverse its Y direction
//    if (ballY + ballR >= GLCD.Bottom || ballY - ballR <= 0 ) 
//      ballDirectionY = -ballDirectionY;
//
//     // if the ball goes off the screen left or right, reverse X direction
//    if (ballX - ballR < 0 || ballX + ballR >= GLCD.Right )
//      ballDirectionX = -ballDirectionX; 
//	  
//     // clear the ball's previous position:
//    //GLCD.DrawCircle(ballX, ballY,ballR, WHITE);
//    GLCD.FillRect(ballX-ballR-1, ballY-ballR-1, ballX+ballR, ballY+ballR, WHITE);
//   
//     // increment the ball's position in both directions:
//    ballX = ballX + 2 * ballDirectionX;
//    ballY = ballY + 2 * ballDirectionY;
//	  
//    GLCD.FillCircle(ballX, ballY, ballR, BLACK);
//    delay(30 );
//  }
//}
