# frames.py

from lcd_objects import *
import lcd, log
from datetime import datetime

class Frame:
	def __init__(self):
		self.objs = []
		pass

	def getCmd(self):
		cmd = ""
		for obj in self.objs:
			cmd += obj.getCmd()
		return cmd

class statusBar(Frame):
	def __init__(self):
		self.clearClock = ClrA(95, 1, 128-95-2, 5)
		self.statusTitle = Text("APP DRAWER", 4, 1, 100, 40, 1)
		self.clockText = Text("00:00:00", 95, 1, 128-95-1, 40, 1)
		status = Inv(0, 0, 128, 7);
		
		# self.objs = [statusTitle, clockText, status]
		self.objs = [self.clearClock, self.statusTitle, self.clockText]

		# x = Frame()
		# x.objs = [status]
		# lcd.flushFrame(x)

	def timeupdate(self):
		log.Info("updating time")
		tstr = datetime.now().strftime("%H:%M:%S")
		self.clockText.set({"msg": tstr})


class statusBarFrame(Frame):
	def __init__(self):
		self.statusBar = statusBar()

	def timeupdate(self):
		self.statusBar.timeupdate()

	def setTitle(self, title):
		self.statusBar.statusTitle.set({"msg":title})


class iconMenu(statusBarFrame):
	def __init__(self):
		statusBarFrame.__init__(self)
		
		tWidth = 40
		app_x = 0;
		self.app_h = 18
		txt_h = 18
		h=50
		self.pad=1
		self.rat = 126/3

		consPad = (self.rat-25)/2
		self.app1 = Img(5, 0+1+consPad, 18)
		self.name1 = Text(self.getTitle("QUIZ"), 0+1, 51, app_x+tWidth, self.app_h+h, 1)
		self.app2 = Img(2, 0+1+self.rat+self.pad+consPad, 18)
		self.name2 = Text(self.getTitle(" GAMES"), 0+1+self.rat+self.pad, 51, app_x+2*tWidth, self.app_h+h, 1)
		self.app3 = Img(1, 0+1+2*(self.rat+self.pad)+consPad, 18)
		self.name3 = Text(self.getTitle("SETTINGS"), 0+1+2*(self.rat+self.pad), 51, app_x+3*tWidth, self.app_h+h, 1)

		self.high = Inv(0, self.app_h-2, self.rat-2, self.rat+2)
		
		self.clr = ClrA(0, 14, 128-1, 64-14-1)
		self.objs = []
		self.objs.extend([self.clr, self.app1, self.name1, self.app2, self.name2, self.app3, self.name3, self.high])


	def getTitle(self, tit):
		# assumes title font of 3x5
		char_w = 4.1
		self.rat=126/3
		maxChars=self.rat/char_w
		strLen = len(tit)
		left = maxChars - strLen
		toPadBy = left/2
		strOut = ''

		for i in range (0, int(toPadBy)):
			strOut += ' ' 
		strOut += tit
		for i in range (0, int(toPadBy)):
			strOut += ' ' 
		return strOut;

	def highLightApp(self, index):
		Inv((index*126)/3+index*self.pad, self.app_h-2, self.rat, self.rat)
		self.objs[-1].set ({
			'x': (index*126)/3+index*self.pad
		})	

	def setApp(self, ind, name, iconId):
		iconInd = 2*ind+1; textInd = 2*ind+2
		self.objs[iconInd].set({"Id":iconId})
		self.objs[textInd].set({"msg":name})



class ListRow(statusBarFrame):
	def __init__(self):
		statusBarFrame.__init__(self)
		self.base = 14
		self.pad = 6
		self.pad_h = 3
		self.row_pad = 1
		self.char_h = 7
		self.char_w = 6
		self.h = self.char_h + 2*self.pad_h + self.row_pad
		self.high = Inv(0,self.base, 127, self.h) # Insert in the end after populating the list by calling finishList

	def getRow(self, index, tit, stat):

		lText = Text(tit, self.pad, self.base+index*(self.h+self.row_pad)+self.pad_h, 127-self.pad-2, self.h, 4)
		rText = Text(stat, 127 - len(stat)*self.char_w-3, self.base+index*(self.h+self.row_pad)+self.pad_h, len(stat)*self.char_w+2, self.h, 4)

		return [lText, rText]

	def highlightRow(self, index):
		self.high.set({
			"y" : self.base+index*(self.h+self.row_pad)
		})

	def finishList(self):
		self.objs += [self.high]



class Settings(ListRow):
	def __init__(self):
		ListRow.__init__(self)
		
		self.clr = ClrA(0, 14, 128-2, 64-14-2)
		self.objs = [self.clr]
		self.objs += self.getRow(0, 'Swamy', '< Y >')
		self.objs += self.getRow(1, 'Raj', '< N >')
		self.objs += self.getRow(2, 'Mux', '< N >')
		self.finishList()

	def setRows(self, rows):
		def Y_N(b):
			return '< Y >' if b else '< N >'

		self.objs = [self.clr]
		i = 0
		for row in rows:
			self.objs += self.getRow(i, row[0], Y_N(row[1]) )
			i=i+1
		
		self.finishList()



class Quiz(statusBarFrame):
	def __init__(self):
		statusBarFrame.__init__(self)
		self.qText = Text("DEFAULT MESSAGE", 2, 15, 125, 40, 2)
		self.rect = Rect(1, 50, 126, 14, 1, 1)
		self.eText = Text("Enter answer here", 10, 50+3, 117, 14, 2)
		
		self.clr = ClrA(0, 14, 128-2, 64-14-2)
		self.objs = [self.clr]
		self.objs += [self.qText, self.rect, self.eText]
		# x = LcdObj()
		# lcd.flushFrame(x)

	def setText(self, textMessage):
		self.qText.set({
			"msg" : textMessage
		})


# class Welcome(statusBarFrame):
# 	def __init__(self):
# 		statusBarFrame.__init__()
# 		line = Hline(0, 50, 128)
# 		qtext = Text("message", 0, 0, 80, 40, 2)
# 		self.objs.append([line, qtext])		# rendering pipeline..

# 		self.QUESTION = qtext
        