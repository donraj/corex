# fake-RBC.py
from fakeSerial import *
from Tkinter import *
import threading, time
import globe

MAX_CHARS = 16
delim = '\n'

class Serial(fakeSerial):
	def __init__(self, port='/dev/ttyUSB1', baudrate=9600, timeout=1, handle = None):
		""" The fake constructor
		"""
		fakeSerial.__init__(self, port, timeout)
		global delim
		self.SIG = "RBC-fake "
		self.delim = delim
		self._data = self.SIG 				# Initially this is served
		self._receivedData = ''
		self.display = handle

	def action(self, cmd):
		print 'command = ' + cmd
		if (cmd[0] == 'w'):
			self._data += self.SIG
		elif (cmd[0] == 'd'):
			self.display(cmd[1:])		# display
		pass

	def process(self):
		if (len(self._receivedData) == 0):
			return

		l = self._receivedData.split(self.delim)
		if (l[-1] == ''):
			self._receivedData = ''
		else:
			self._receivedData = l[-1]
		
		l = l[:-1]
		for cmd in l:
			self.action(cmd)

	def write( self, string ):
		self._receivedData += string
		self.process()

	def close(self):
		pass	

