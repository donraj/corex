# fakeSerial.py
# D. Thiebaut
# A very crude simulator for PySerial assuming it
# is emulating an Arduino.
import time

# a Serial class emulator 
class fakeSerial:
    ## init(): the constructor.  Many of the arguments have default values
    # and can be skipped when calling the constructor.
    def __init__(self, port='COM1', timeout=1):
        self.name     = port
        self.port     = port
        self.timeout  = timeout
        self._isOpen  = True
        self._receivedData = ""
        self._data = ""

    ## isOpen()
    # returns True if the port to the Arduino is open.  False otherwise
    def isOpen( self ):
        return self._isOpen

    ## open()
    # opens the port
    def open( self ):
        self._isOpen = True

    ## close()
    # closes the port
    def close( self ):
        self._isOpen = False

    ## write()
    # writes a string of characters to the Arduino
    # It would be nice to overload this one...
    def write( self, string ):
        print( 'Arduino got: "' + string + '"' )
        self._receivedData += string

    ## read()
    # reads n characters from the fake Arduino. Actually n characters
    # are read from the string _data and returned to the caller.
    def _read( self, n=1 ):
        s = self._data[0:n]
        self._data = self._data[n:]
        return s

    # respects the timeout of the serial port
    def read(self, n=1):
        t = self.timeout/10.0
        for i in range(0, 10):
            ans = self._read(n)
            if (ans != ''):
                return ans
            time.sleep(t)
        return ''

    ## readline()
    # reads characters from the fake Arduino until a \n is found.
    # We are not implementing this as readline is not supported by pySerial on all platforms
    # def readline( self, delim = '\n' ):
    #     returnIndex = self._data.index( "\n" )
    #     if returnIndex != -1:
    #         s = self._data[0:returnIndex+1]
    #         self._data = self._data[returnIndex+1:]
    #         return s
    #     else:
    #         return ""

    ## __str__()
    # returns a string representation of the serial class: Totally FAKE :D
    def __str__( self ):
        return  "Serial<id=0xa81c10, open=%s>( port='%s', baudrate=%d," \
               % ( str(self.isOpen), self.port, self.baudrate ) \
               + " bytesize=%d, parity='%s', stopbits=%d, xonxoff=%d, rtscts=%d)"\
               % ( self.bytesize, self.parity, self.stopbits, self.xonxoff,
                   self.rtscts )

