from gluon import DAL, Field
import os
import rbc

# def test():
# 	print ("hello from db test. I shall be making tables and what not...")
# 	cwd = os.getcwd()
# 	path = os.path.join(cwd, 'test', 'testdb')
# 	print path
# 	db = DAL('sqlite://database.db', folder=path)
# 	person = db.define_table('person',Field('name','string'))
# 	print "if you see this, means database is working well"
# 	rbc.display('db - ok')



cwd = os.getcwd()
path = os.path.join(cwd, 'test', 'testdb')
db = DAL('sqlite://database.db', folder=path)
settings = db.define_table('settings',Field('name','string'),Field('option','string'))


def insert(name, option):
	db.settings.insert(name=name, option= option)
	db.commit()

def get_all():
	all_rows = db(db.settings).select()
	return all_rows

def update(name, option):
	db(db.settings.name == name).update(option= option)
	db.commit()


def clear():
	db(db.settings).delete()
	db.commit()

def reset():
	clear()
	insert("Sound", "True")
	insert("Wifi", "False")
	insert("Bluetooth", "True")




