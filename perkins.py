# This module interfaces with Core X only
import threading, time, globe, log, serial
from events import *

PORT = ""			# This is like /dev/ttyUSBx, where X is an integer
CIN_TIMEOUT = 1     # Plase do not make this None, else listen thread shall load CPU

Conn = None
SysFunc = None		# The CoreX function to call in case of an event received from PERKINS
WORK = True

keycode_perkins = -1 

def begin(function):
	"""Kind of constructor for this module. Core scan """
	makeBrailleMap()
	global PORT, Conn, SysFunc, WORK, CIN_TIMEOUT
	SysFunc = function
	PORT = globe.ports[globe.PERKINS]
	try:
		Conn = serial.Serial(str(PORT), 9600, timeout = CIN_TIMEOUT)	
	except:
		log.pr("Perkins Failed to conn, port name: " + str(PORT))
		return False

	WORK = True
	listenThread = threading.Thread(target=listener)
	listenThread.start()
	return True

def end():
	"""Stops the listener
	"""
	global WORK
	WORK = False
	pass

# Define mappings from keycodes to keypresses! -> Maybe a csv file?
def getKeyCode(i):
	if (i & PERKINS_ENTER):
		return ord('\r')	# Ascii code of carriage return

	if (i & PERKINS_SPACE):
		return ord(' ')

	if (i & PERKINS_BCK):
		return ord('\b')   # Ascii code of backspace

	# USE the braille2ascii mapping here :p
	return braille2ascii[i]

def process(inp):
	global SysFunc, keycode_perkins
	if ((len(inp) < 1) and (len(inp) > 5)):
		return

	if (inp[0] == 'r'):
		if (keycode_perkins < 0):
			return
			
		e2 = event(DEV_PERKINS, keycode_perkins, KEY_PRESS)		
		SysFunc(e2)
		return

	if (inp[0] == 'p'):
		try:
			keycode_perkins = getKeyCode(int(inp[1:]))
		except ValueError:
			log.log("Perkins gave bad output")
			return
		# e = event(DEV_PERKINS, keycode_perkins, KEY_DOWN)
		# SysFunc(e)
	pass

def listener():
	global Conn
	st = ""
	while(WORK):
		if (len(st)>10):
			st = ""  # something bad has happened!
		c = Conn.read()  # This shall be blocking internally, so processor resources shall not be used. PLEASE DO NOT USE "None" timeout
		if (c == ' '):
			process(st)
			st = ""
			continue
		st = st + c
		
	Conn.close()
	pass


def makeBrailleMap():
	global braille2ascii, ascii2braille
	f = open('sysData/brailleMap.txt', 'r')
	for line in f:
		l = line.split('\t')
		l2 = l[1].split('-')

		asciiCode = ord(l[0])
		brailleCode = 0
		for dig in l2:
			brailleCode += (1<<(int(dig)-1))

		braille2ascii[brailleCode] = asciiCode
		ascii2braille[asciiCode] = brailleCode
