#include <openGLCD.h>
#include <bitmaps/BillsHead.h>
#include <bitmaps/ArduinoIcon64x64.h>
#include <bitmaps/ArduinoIcon96x32.h>

#define DELAY 2000 // ms delay between examples

#include "fonts/allFonts.h"         // system and arial14 fonts are used
//#include "bitmaps/ic_contacts.h"
#include "mail.h"
#include "settings.h"
#include "games.h"
#include "quiz1.h"
#include "quiz2.h"
#include "question.h"

#define SIG "LCD"
#define DELIM '~' // this is actually the last arg..
#define COMMA_DELIM '`';

/*
commands: - lets be idiots...
	FORMAT: command <b> <arg1> <b> <arg2> ... <b>~<b>
					<b> => ` character. NOTE the <b> at the end.
					example:
					"clr ~ " 
					"rect 40 50 10 10 ~ "
					
					-> think of ~ as the last argument.

		#### NOTE ### The delimiter ` is being represented by space here..
	LIST:
		1. clr ~ : clear the screen.
		2. img i x y ~ : draw ith image with x, y as left-top corner.
		3. vl x y h ~ :  vertical line at x, y of h height.
		4. hl x y w ~ : horizontal line at x, y of w width.
		5. rect x y w h IsRound r ~ : a rectangle at x, y with w: width, h: height, r: radius; IsRound != 0 for rounded.
		6. write str x y w h ~ : write str in the rect area.
                7. clrA x y w h ~ : clear the text area.
		8. font i ~ : set font size to i. (if we have that).
		9. inv x y w h ~ : inverts the rectangle!
*/

const int CMAX = 100;
char cmd[10][CMAX];  // cmd[0] is the command, rest are arguments...

int geti(int i){
	if (cmd[i][0] == '~')
		return -1;

	String s = cmd[i];
	return s.toInt();
}

void processCmd(){
	 String C = String(cmd[0]);
	 if (C == "~") return;
        
         if (C == "w"){
           Serial.write(SIG);
           Serial.write("\n");
         }
           
	 if (C == "clr"){
		GLCD.ClearScreen();
		return;
	 }

	 if (C == "img"){
		int ind = geti(1), x = geti(2), y = geti(3);
		// Serial.println(String(ind) + " " + String(x) + " " + String(y));
		switch(ind){
			case 0: GLCD.DrawBitmap(mail, x, y); break;
			case 1: GLCD.DrawBitmap(settings, x, y); break;
			case 2: GLCD.DrawBitmap(games, x, y); break;
			case 3: GLCD.DrawBitmap(quiz1, x, y); break;
			case 4: GLCD.DrawBitmap(quiz2, x, y); break;
			case 5: GLCD.DrawBitmap(question, x, y); break;
		}
		return;
	 }

	 if (C == "vl"){
		int x = geti(1), y = geti(2), h = geti(3);
		GLCD.DrawVLine(x, y, h);
		return;
	 }

	 if (C == "hl"){
		int x = geti(1), y = geti(2), w = geti(3);
		GLCD.DrawHLine(x, y, w);
		return;
	 }

	 if (C == "rect"){
		int x = geti(1), y = geti(2), w = geti(3), h = geti(4), isR = geti(5);
		if (isR == 0)
			GLCD.DrawRect(x, y, w, h);
		else{
			int r = geti(6);
			if (r < 0)
				r = 5;
			GLCD.DrawRoundRect(x, y, w, h, r);
		}
		return;
	 }

	 if (C == "write"){
		int x = geti(2), y = geti(3), w = geti(4), h = geti(5), fsz = geti(6);
		gText t = gText(x, y, x + w, y + h);
                switch(fsz){
                  case 1: t.SelectFont(Wendy3x5); break;
//                  case 2: t.SelectFont(System4x6); break;
//                  case 3: t.SelectFont(System5x6); break;
//                  case 4: t.SelectFont(System6x7); break;
                
                  default: t.SelectFont(System5x7);break;  
                }
		
		t.Printf(cmd[1]);
		return;
	 }

         if (C == "clrA"){
           int x = geti(1), y = geti(2), w = geti(3), h = geti(4);
           GLCD.FillRect(x, y, w, h, PIXEL_OFF);
//	   gText t = gText(x, y, x + w, y + h);
//           t.ClearArea();
           return;
         }

	 if (C == "font"){
         
	  return;
	 }

	 if (C == "inv"){
	 	int x = geti(1), y = geti(2), w = geti(3), h = geti(4);
	 	GLCD.InvertRect(x, y, w, h);
		return;
	 }

}


void printCmd(){
	Serial.print("cmd = ");

	int i=0;
	while (cmd[i][0] != '~' && i < 10){
		Serial.print(cmd[i]);
		Serial.print(" ");
		i++;
	}
	
	Serial.println("\n");
}

void setup(){
	GLCD.Init();
	GLCD.ClearScreen();  
	GLCD.SelectFont(System5x7, BLACK);       // default font.

	Serial.begin(9600);
	Serial.setTimeout(50);            // reading timeout = 1/2 seconds. perhaps not-enough
	cmd[0][0] = '~';                    // clear the cmd.
	Serial.write(SIG);
        Serial.write("\n");
        
        gText t = gText(10, 20, 10 + 90, 20 + 30); 
        t.SelectFont(Roosewood22);
        t.Printf("welcome");
}

void loop(){
	while (!Serial.available()){
		delay(5);  // a small delay - required??
	}
	delay(5);
	boolean x = get_cmd();
	if (x == true){
//			printCmd();          
			processCmd();
	}
}


boolean get_cmd(){
	for (int i=0; i<10; i++)
		cmd[i][0] = '\0';

	int i=0;
	while ( i < 10 ){
		int l = Serial.readBytesUntil('`', cmd[i], CMAX - 1);
		if (l <= 1) l = 1;
		cmd[i][l] = '\0';
		
		if (cmd[i][0] == '~'){
			i++;
			break;
		}
		
		i++;
	}
	
	if (cmd[i-1][0] == '~')
		return true;
	
	cmd[0][0] = '~';
	cmd[0][1] = '\0';
	return false;
}

