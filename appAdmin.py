# appAdmin.py
# Provides functions to figure out the status of the applications installed on the device.
	# what is an app??
	# should be a folder in apps/
	# should have begin() and listen(...) functions

import os, subprocess
from glob import *
from test import db_test

def _checkApp(path, string):
	"""Please give the path to main.py"""
	try:
		res = subprocess.check_output('grep "%s" "%s"; exit 0' %(string, path),  shell = True)
		if string in res:
			return True
		return False
	except:
		return False

def getApps():
	s = os.getcwd()
	appList = glob(s+'/apps/*/main.py')
	appList = [app for app in appList if _checkApp(app, 'begin')]
	appList = [app for app in appList if _checkApp(app, 'listen')]
	appList = [app.replace(s+'/apps/','', 1) for app in appList]
	appList = [app.replace('/main.py','', 1) for app in appList]
	return appList



def getAppIcon(appName):
	"""
		Returns an integer, which is the icon id of the application.
	"""
	string = "__ICON"
	path = os.getcwd() + '/apps/' + appName + '/main.py'
	res = subprocess.check_output('grep "%s" "%s"; exit 0' %(string, path),  shell = True)
	if (len(res) < 2):
		return -1	# use some default instead	
	return int(res.strip().split(' ')[-1])


