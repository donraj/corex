import subprocess
import time
from random import randint


def speech(text,language,gender,words_per_min,speed,delay):
	'''
    Says the speech; (It first stores and then plays it becauseotherwise when done directly espeak causes interruptions.)
    '''
	p = subprocess.Popen('mkdir tmp 2>>/dev/null', stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()
	r = randint(1,1000)
	address = 'tmp/'+str(r)+'.wav';
	a = 'espeak -v'+language+'+'+gender+'5 -s'+str(words_per_min)+' -w '+address+' "%s" 2>>/dev/null' % text
	p = subprocess.Popen(a, stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()
	time.sleep(delay)
	proc = subprocess.Popen(["play",address,"speed",str(speed)]);
	time.sleep(0.5)
	remove_speech(address)
	return proc

def stop(process):
	'''
    Stops the speech
    '''
	process.kill()
	
def check_alive(process):
	'''
    Polls and check if the current process is still alive.
    '''
	if(process.poll()==0):
		return 0;
	else:
		return 1;

def clear_tmp():
	'''
    Clears the tmp directory
    '''
	a = 'rm -rf tmp'+ ' 2>>/dev/null'
	p = subprocess.Popen(a, stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()

def store_speech(text,language,gender,speed,wav_name):
	'''
	stores the speech at current directory in form of .wav file
	'''
	a = 'espeak -v'+language+'+'+gender+'5 -s'+str(speed)+' -w '+wav_name+' "%s" 2>>/dev/null' % text
	p = subprocess.Popen(a, stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()

def play_speech(wav_name,speed,delay):
	'''
	Plays speech from a .wav file
	'''
	a = 'play '+wav_name+' speed '+str(speed)+ ' 2>>/dev/null'
	time.sleep(delay)
	p = subprocess.Popen(a, stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()

def remove_speech(wav_name):
	'''
	removes the .wav file
	'''
	a = 'rm '+wav_name+ ' 2>>/dev/null'
	p = subprocess.Popen(a, stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()