# Filename: core.py - Remember, this is a script: Does the Big Bang
# import sysButtons
import time
import initDevices, perkins, log, rbc, lcd,  appAdmin, fakeDevices
from globe import *
from events import *
from PyQt4 import QtGui, QtCore   # for taking keyboard input
import threading, time, sys
from test import db_test
import os, imp
from lcd_frames import *

if not(FAKE):
	import sysButtons


fakeRBC_disp = None		# the pyqt gui handle for the fake rbc
currentApp = None		# should be set to admin by default
_applist = None
nextApp = None

def popat(x):
	print "The core was successfully challenged %d " %x 
	pass

def registerApp(handle):
	global currentApp
	currentApp = handle
	pass

def listenInp(e):
	""" Called by the perkins & sysButtons module, upon an input event
		e is of type class event
	"""
	print (str(e))
	if (e.dev == DEV_PERKINS):
		print str(e) + " " + repr(chr(long(e.keycode))) + " " + str(ascii2braille.get(e.keycode, -1))	# the object of type event is taken care of here.

	if (currentApp != None):
		currentApp(e)

	pass


# The entry point of the system: The big-bang
def begin():
	global fakeRBC_disp

	if (FAKE == True):
		fakeDevices.begin()	 # this is only for RBC actually.

	initDevices.begin()

	if (ports[RBC] != None):
		res = rbc.begin(fakeRBC_disp)
		log.pr("rbc initialization " + str(res))
		time.sleep(0.3)
	
	if (ports[PERKINS] != None):
		res = perkins.begin(listenInp)
		log.pr("Perkins inititlization " + str(res))

	if (ports[LCD] != None):
		res = lcd.begin()
		log.pr("Lcd inititlization " + str(res))	
		if (res):
			print("shall wait for a moment")
			time.sleep(0.3)

	if (not(FAKE)):
		sysButtons.begin(listenInp)
	# res = sysButtons.begin(listenInp)
	# log.pr("sysbuttons iniliazed -- actually hardware status unknown " + str(res))


	# frame = Welcome()
	# lcd.flushFrame(frame)

	# rbc.display('core ok')

	global _applist
	_applist = appAdmin.getApps()
	log.Info("Installed apps are\n" + repr(_applist))

	# Call the admin application...
	switchApp(0)
	changeApplication()
	
	db_test.test()

	# The code here shall pass over to adminApp (not appAdmin) which shall take over...
	# adminApp is a normal application, which shall manage other applications
	# adminApp could have been integrated into core.py, but just for personal bias imbibed from my Italian Friend 'Massimo Di Pierro' its kept separate.
	# The adminApp is located in apps/.admin
	# BEFORE DYING PLEASE DO rbc.end() specially in case of virtual rbc (mTkinter one)
	# rbc.display('Quiting(5)')

	# time.sleep(15)
	# rbc.end()

def getAppList():
	return _applist

def switchApp(index):
	global nextApp
	nextApp = index


def changeApplication():
	# while (True):
	curr = nextApp
	app_path = os.path.join(os.getcwd(), 'apps', _applist[nextApp], 'main.py')
	app = imp.load_source('app', app_path)
	registerApp(app.listen)
	app.begin()
	# log.Info("app " + _applist[curr] + " closed gracefully")

	# possible memory leak...



# This is effectively our application
class MasterThread(QtCore.QThread):
	def __init__(self, parent):
		QtCore.QThread.__init__(self,parent)
		self.parent = parent
		self.connect(parent, QtCore.SIGNAL("keyPress"), self.KeyListener)

	def KeyListener(self, e):
		""" Here it has to generate the right event as per out events.py
			And call the listen inp of core.. which is on top of this page.
		"""

		key = e.key()
		# log.Error( str(key) + " " + str(e.type))

		SysKeyCodes = [KEYCODE_BCK,KEYCODE_UP,KEYCODE_DOWN,KEYCODE_LEFT,KEYCODE_RIGHT,KEYCODE_OK]
		if(int(key) in SysKeyCodes):
			cur_keycode = int(key)
			dev_device = DEV_SYS

		elif (int(key) == 16777219):
			cur_keycode = ord('\b')
			dev_device = DEV_PERKINS			
		else:
			cur_keycode = int(key)
			dev_device = DEV_PERKINS

		board_event = event(dev_device, cur_keycode, KEY_PRESS)
		listenInp(board_event)
		print str(e.key())
		
	def run(self):
		self.display('starting')
		global fakeRBC_disp
		fakeRBC_disp = self.display
		begin()

	def display(self, msg):
		self.emit(QtCore.SIGNAL("updateRBC"), msg)


# This is the fake thing...
class KeyListener(QtGui.QMainWindow):
	def __init__(self):
		super(KeyListener, self).__init__()
		self.initUI()
		self.thread = MasterThread(self)
		self.connect(self.thread, QtCore.SIGNAL("updateLCD"), self.updateLCD)
		self.connect(self.thread, QtCore.SIGNAL("updateRBC"), self.updateRBC)
		self.thread.start()

		
	def initUI(self):      
		self.text = QtGui.QTextEdit(self)
		self.text.setReadOnly(True)
		self.text.setLineWrapMode(QtGui.QTextEdit.NoWrap);
		self.text.setFont(QtGui.QFont('Courier', 40))
		self.text.move(10, 10)
		self.text.resize(256, 70)

		self.statusBar()
		self.setGeometry(300, 300, 256+20, 128+100+20)
		self.setWindowTitle('Emulator')

		self.text.setFocusPolicy( QtCore.Qt.NoFocus )
		self.show()
		
	def keyPressEvent(self, e):
		self.statusBar().showMessage(str(e.key()) + ' was pressed')
		self.emit(QtCore.SIGNAL("keyPress"), e)

		if e.key() == QtCore.Qt.Key_F4:
			self.close()
	
	def updateLCD(self, msg):
		print 'Update on LCD: ' + msg
		self.text.setText(msg)

	def updateRBC(self, msg):
		print 'Update on RBC: ' + msg
		self.text.setText(msg)


def fakeBegin():
	app = QtGui.QApplication([''])
	ex = KeyListener()
	sys.exit(app.exec_())


if __name__ == "__main__":
	if (FAKE == True):
		fakeBegin()
	else:
		begin()
