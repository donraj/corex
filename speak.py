# The simple audio module:
# There are only 2 functions exposed: playFile and playText

from subprocess import *

global process_espeak, process_sox
def _kill():
	global process_espeak, process_sox
	try:
		process_sox.kill()
	except:
		pass
	try:
		process_espeak.kill()
	except:
		pass

def playFile(filepath):
	global process_espeak, process_sox
	_kill()
	process_sox = Popen(["play", filePath], stderr = open('/dev/null'))

def playText(msg, speed = 140, voice = 'default'):
	""" For more voices type espeak --voices
		Speed = 150 is chosen arbitrarily; the standard is actually higher
	"""
	global process_espeak, process_sox
	_kill()
	process_espeak = Popen(["espeak", "-v", voice, "-s", str(speed), msg], stdout=PIPE)
	process_sox = Popen(["play", "-"], stdin=process_espeak.stdout, stderr=open('/dev/null'))
