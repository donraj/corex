/* quiz1 bitmap file for openGLCD library */
/* Bitmap created from quiz1.png      */
/* Date: 7 May 2015      */
/* Image Pixels = 625    */
/* Image Bytes  = 100     */


#ifndef quiz1_H
#define quiz1_H

GLCDBMAPDECL(quiz1) = {
  25, // width
  25, // height

  /* page 0 (lines 0-7) */
  0xf8,0xfc,0xfe,0x7,0x3,0x83,0x83,0x87,0xfe,0xfc,0x60,0x0,0x0,0x0,0x0,0x0,
  0x0,0x24,0x4,0x4,0x4,0x4,0x4,0x24,0x4,
  /* page 1 (lines 8-15) */
  0x0,0x3,0x7,0x7,0x86,0xc6,0xc7,0x87,0x7,0xf,0xc,0x0,0x6,0x6,0x0,0x0,
  0x0,0x82,0x82,0x82,0x82,0x82,0x82,0x82,0x2,
  /* page 2 (lines 16-23) */
  0x0,0x80,0xf0,0xfc,0x7f,0x63,0x61,0x7f,0xfe,0xf8,0xc0,0x0,0x80,0xc0,0x0,0x0,
  0x0,0x48,0x8,0x8,0x8,0x8,0x8,0x48,0x8,
  /* page 3 (lines 24-31) */
  0x0,0x0,0x1,0x0,0x0,0x0,0x0,0x0,0x0,0x1,0x1,0x0,0x1,0x1,0x0,0x0,
  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
};
#endif
