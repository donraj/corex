# Filename: sysButtons.py - sister module of perkins.py
import RPi.GPIO as GPIO
from events import *
import time

# pin constants - CHANGE AS PER CONFIGURATION
# Put the right combination here...
BUTT = {14:KEYCODE_UP, 15:KEYCODE_DOWN, 18:KEYCODE_LEFT, 23:KEYCODE_RIGHT, 24:KEYCODE_OK, 25:KEYCODE_BCK}

SysFunc = None
# need a test-board to test!!
	# Test Board made... testing all maska

def begin(function):
	global SysFunc, BUTT
	SysFunc = function
	GPIO.setmode(GPIO.BCM)

	for b in BUTT.keys():
		GPIO.setup(b, GPIO.IN, pull_up_down=GPIO.PUD_UP)
		GPIO.add_event_detect(b, GPIO.FALLING, callback=press, bouncetime=200)

	return True

def end():
	global SysFunc, BUTT
	for b in BUTT.keys():
		GPIO.remove_event_detect(b)
	SysFunc = None

def press(keycode):
	global SysFunc, BUTT, timestamp_quit
	if (keycode in BUTT.keys()):
		e = event(DEV_SYS, BUTT[keycode], KEY_PRESS)
		SysFunc(e)
