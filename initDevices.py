# initCore.py
# THE HARDWARE MODULE
import serial, os, log, time, subprocess, globe

delim = globe.delim

def readLine(s, sz, delim):
	st = ""
	counter = 0
	for i in range(0, 7):		# repeats at most 7 times = 7 seconds at most!!
		if (len(st)>=sz):
			return st
		c = s.read()			# This can take atmost 1 seconds
		if (c == delim):
			return st
		st = st + c
	return st

def sigCheck(sig, portComm):
	"""Checks for the signature for the present device signatures
		and initializes the corresponsing global state variable
	"""
	for s in globe.SIG:
		if (s in sig):
			globe.ports[globe.SIG.index(s)] = portComm.port
			return True
	return False

def getSig(portName):
	global delim
	try:
		s = serial.Serial(portName, 9600, timeout = 1)
	except:
		log.pr("can't connect to port: " + str(portName))
		return ""

	time.sleep(0.5)  #need to give some time to atmega-8 to initialize! 
	s.write('w' + delim)
	resp = readLine(s, 10, ' ')
	result = sigCheck(resp, s)
	s.close()
	return resp

def begin():
	ports = subprocess.check_output('ls ' + globe.ttyMap + '; exit 0',  shell = True)
	if (len(ports) < 1):
		log.pr("No devices connected. Aborting")
		pass

	print "ports:\n" + ports
	ports = ports.split('\n')
	for port in ports:
		if ('bluetooth' in port.lower()):
			continue
		
		print "querying: " + port
		if (len(port) <= 1):
			continue

		sig = getSig(port)
		log.pr('sig = ' + sig)

def flushAll():
	for port in ports:
		port.close()

