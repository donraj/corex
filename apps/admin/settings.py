# settings.py - the subapp of main app

import rbc, lcd, log, time, speak
from events import *

from lcd_frames import *
from datetime import datetime
from apps.admin.utils import *

import apps.admin.database.database as db

SETUP = None
CurFrame = None
FLUSH_IT = False

INDEX = 0
DATA = None

def loadData():
	global DATA
	rows = db.get_all()
	DATA = []
	for row in rows:
		DATA.append((row.name, (True if (row.option == "True") else False ) ))


def setup():
	global SETUP, CurFrame, FLUSH_IT
	loadData()

	if (SETUP != None):
		FLUSH_IT = True
		return

	
	lcd.flushFrame( LcdObj() )
	SETUP = True
	CurFrame = Settings()
	CurFrame.setTitle("SETTINGS")
	CurFrame.setRows(DATA)
	FLUSH_IT = True


def update():
	global CurFrame, FLUSH_IT
	CurFrame.setRows(DATA)
	CurFrame.highlightRow(INDEX)
	FLUSH_IT = True


count = -1
def loop():
	global FLUSH_IT, count
	
	if (FLUSH_IT):		
		CurFrame.setRows(DATA)
		lcd.flushFrame(CurFrame)

	if (FLUSH_IT or count < 0):
		count = 13
		timeupdate(CurFrame)
	
	if (FLUSH_IT):
		rbcDisp(DATA[INDEX][0] + (" Y" if DATA[INDEX][1] else " N") , False)
		speak.playText(DATA[INDEX][0] + (" on" if DATA[INDEX][1] else " off") )
		FLUSH_IT = False

	count = count - 1

def scrollUp():
	global INDEX
	INDEX += 1
	if (INDEX >= 3):
		INDEX = 2

def scrollDn():
	global INDEX
	INDEX -= 1
	if (INDEX < 0):
		INDEX = 0

def select():
	global DATA
	DATA[INDEX] = ( DATA[INDEX][0], not(DATA[INDEX][1]) )
	db.update( DATA[INDEX][0],  str(DATA[INDEX][1]) )
	update()

def pseudoListen(e):
	log.Error("settings: " + str(e))
	if (e.dev == DEV_SYS):
		if (int(e.keycode) == KEYCODE_UP and not(FLUSH_IT) ):
			scrollDn()
			update()

		elif (int(e.keycode) == KEYCODE_DOWN and not(FLUSH_IT) ):
			scrollUp()
			update()

		elif (int(e.keycode) == KEYCODE_OK and not(FLUSH_IT) ):
			select()

	pass


