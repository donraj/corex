# utils: common across subapps.
import rbc, lcd, log, time, speak
from events import *

from lcd_frames import *

def timeupdate(fr):
	fr.timeupdate()	# should actually check if timeupdate
	lcd.flushFrame(fr.statusBar)


def rbcDisp(word, Speak = True):
	rbc.display(word[0:16])
	if (Speak):
		speak.playText(word)