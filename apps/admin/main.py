# The admin app. This is actually just an application! 
# But it manages other applications

import rbc, lcd, log, time, speak
from events import *

from lcd_frames import *
from datetime import datetime
import apps.admin.settings as settings
from apps.admin.utils import *
import apps.learn.main as learn

CurFrame = None
MainMenu = [None, None]

IconIndex = 0
FLUSH_IT = False
SELECT = False

SUB_APP = -1	# the application is itself...


TITLE = ["quiz", "games", "settings", "alpha", "beta", "gamma"]

def processWord():
	global currWord
	display(currWord)
	currWord = ""


def setup():
	""" 
		Creates the startup frame(s) for the first time...
		Initializes some global vars in this context...
	"""
	global MainMenu, CurFrame, IconIndex
	MainMenu[0] = iconMenu()
	MainMenu[0].setTitle("APP DRAWER (1)")
	MainMenu[0].setApp(0, "   QUIZ", 3)
	MainMenu[0].setApp(1, "  GAMES", 2)
	MainMenu[0].setApp(2, " SETTINGS", 1)
	MainMenu[0].highLightApp(0)

	MainMenu[1] = iconMenu()
	MainMenu[1].setTitle("APP DRAWER (2)")
	MainMenu[1].setApp(0, "  ALPHA", 5)
	MainMenu[1].setApp(1, "  BETA", 4)
	MainMenu[1].setApp(2, "  GAAMA", 0)

	CurFrame = MainMenu[0]
	CurFrame.highLightApp(IconIndex)



def update():
	global CurFrame, FLUSH_IT, MainMenu
	if (IconIndex<3):
		CurFrame = MainMenu[0]
		CurFrame.highLightApp(IconIndex)

	elif (IconIndex>=3 and IconIndex<6):
		CurFrame = MainMenu[1]
		CurFrame.highLightApp(IconIndex - 3)

	FLUSH_IT = True
	# lcd.flushFrame(CurFrame)
	# timeupdate(CurFrame)

def doOtherJob():
	if (SUB_APP == 2):
		settings.loop()
	if (SUB_APP == 0):
		learn.loop()

def begin():
	global FLUSH_IT, CurFrame, MainMenu
	log.Info("The admin app taking over")
	rbc.display('welcome')

	setup()
	lcd.flushFrame(MainMenu[0])
	time.sleep(1)

	FLUSH_IT = True; count = 13
	while (True):
		if (SUB_APP == -1):
			if (FLUSH_IT):
				lcd.flushFrame(CurFrame)
				
			if (FLUSH_IT or count <=0):
				timeupdate(CurFrame)		# see which frame to be sent...
				count = 13

			if (FLUSH_IT):
				# speak.playText(TITLE[IconIndex])
				rbcDisp(TITLE[IconIndex])
				FLUSH_IT = False

			count = count - 1
			time.sleep(0.1)
		else:
			doOtherJob()
			time.sleep(0.2)	# just safety to keep resources...
			# IMPLEMENT THREADING HERE... There is no other option...
			# BIG TODO... QuickFix, make a signaller thread, which shall take back control?...
				# maybe timeout...
		

def indexUp():
	log.Error("up")
	global IconIndex
	IconIndex = IconIndex + 1
	if (IconIndex>5):
		IconIndex = 5

def indexDn():
	log.Error("down")
	global IconIndex
	IconIndex = IconIndex - 1
	if (IconIndex<0):
		IconIndex = 0	

def passEvent(e):
	if (SUB_APP == 2):
		settings.pseudoListen(e)

	if (SUB_APP == 0):
		learn.pseudoListen(e)

# prepares a sub-app to be launched.
def select():
	global SUB_APP
	SUB_APP = IconIndex
	if (SUB_APP == 2):
		settings.setup()
		
	if (SUB_APP == 0):
		learn.setup()


def takeBack():
	global SUB_APP, FLUSH_IT
	SUB_APP = -1
	FLUSH_IT = True

def listen(e):
	log.Info("admin got " + str(e.keycode))

	if (e.dev == DEV_SYS):
		if (SUB_APP != -1):	# means i am not running
			# log.Error(SUB_APP + " " + str(e.keycode))
			if (int(e.keycode) == KEYCODE_BCK):
				takeBack()	# come back to life...
			passEvent(e)

		else:
			# the app is running
			if (int(e.keycode) == KEYCODE_RIGHT and not(FLUSH_IT) ):
				indexUp()
				update()

			elif (int(e.keycode) == KEYCODE_LEFT and not(FLUSH_IT) ):
				indexDn()
				update()

			elif (int(e.keycode) == KEYCODE_OK and not(FLUSH_IT) ):
				select()

	
	if (e.dev == DEV_PERKINS):
		if (SUB_APP != -1):
			passEvent(e)
		else:
			# do something with perkin's input yourself...
			pass

	if (e.keycode == ord('\r') or e.keycode == KEYCODE_OK):
		pass


	pass


def switchApplication(index):
	## THIS IS NOT TRIVIAL: optimization to AVOID stack overflow...
	pass

