# __ICON 7
# __NAME learn
# This is a single file application. 
# takes a word input and displays on the rbc

# The admin app. This is actually just an application! 
# But it manages other applications

import rbc, lcd, log, time, speak
from events import *

from lcd_frames import *
from datetime import datetime
from apps.admin.utils import *
# import settings


FLUSH_IT = False
SELECT = False
currWord = ""

CurFrame = None

QUES = ['What is the first alphabet?',  '1 Gigabye = how many MB', 'Answer to life, Universe and Everything?', 'Bigger when new, gets smaller with use']
ANS = ['A', '1024', '42', 'Soap']
quesIndex = -1;


def processWord():
	global currWord, ANS, quesIndex

	if(currWord.lower() == ANS[quesIndex].lower()):	# Correst Response
		speak.playText("Correct. Next Question")
	else:
		speak.playText("Wrong. The correct answer is " + ANS[quesIndex] + ". Next Question")

	nextQues()
	currWord = ""

def nextQues():
	global quesIndex, QUES, FLUSH_IT, CurFrame
	quesIndex += 1
	if (quesIndex >= len(QUES)): 
		quesIndex = len(QUES)-1	
		speak.playText("No more questions.")
		return

	CurFrame.eText.set({
		'msg' : ''	
	})
	CurFrame.qText.set({
		'msg' : QUES[quesIndex]	
	})
	FLUSH_IT = True
	# display(currWord)

def updateAns():
	global quesIndex, QUES, FLUSH_IT, CurFrame
	CurFrame.eText.set({
		'msg' : currWord
	})
	FLUSH_IT = True


def setup():
	global FLUSH_IT, CurFrame, quesIndex
	log.Info("Quiz app taking over")
	rbc.display('Quiz App')
	CurFrame = Quiz()
	FLUSH_IT = True
	quesIndex = -1
	nextQues()

count = 13
WAS_LAST_KEY_BCK = False
def loop():
	global FLUSH_IT, CurFrame, count, WAS_LAST_KEY_BCK
	if (FLUSH_IT):
		lcd.flushFrame(CurFrame)
		rbcDisp(currWord, False)
		if (len(currWord)>0 and not(WAS_LAST_KEY_BCK)):
			speak.playText(currWord[-1])
		if (WAS_LAST_KEY_BCK):
			speak.playText("Backspace")
			WAS_LAST_KEY_BCK = False
		
	if (count <=0 or FLUSH_IT):
		timeupdate(CurFrame)		# see which frame to be sent...
		count = 13
		FLUSH_IT = False

	count = count - 1
	

def select():
	global SELECT
	SELECT = True


def pseudoListen(e):
	global currWord, WAS_LAST_KEY_BCK
	log.Error(str(e) + "from quiz")

	if (e.dev == DEV_PERKINS):
		keycode = e.keycode
		if (keycode == ord('\b')):
			WAS_LAST_KEY_BCK = True
			currWord = currWord[:-1]
		elif (keycode == ord('\r')):
			processWord()
		else:
			currWord += chr(e.keycode)

		updateAns()
		log.Error("current word = " + currWord)		# testing

	


