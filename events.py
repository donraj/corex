# event py

#dev constants
devices = ['DEV_PERKINS', 'DEV_SYS']
DEV_PERKINS = 0
DEV_SYS = 1

# state constants
states = ['KEY_UP', 'KEY_DOWN', 'KEY_PRESS']
KEY_UP = 0
KEY_DOWN = 1
KEY_PRESS = 2

# keycode constants: Make them ASCII :-/ (for system buttons)
keycodes = ['KEYCODE_BCK', 'KEYCODE_UP', 'KEYCODE_DOWN', 'KEYCODE_LEFT', 'KEYCODE_RIGHT', 'KEYCODE_OK', 'KEYCODE_QUIT']
KEYCODE_BCK = 16777216	# Corresponding escape button in keyboard - huh
KEYCODE_UP = 16777235	# corresponding Up button in keyboard
KEYCODE_DOWN = 16777237	# corresponding Down button in keyboard
KEYCODE_LEFT = 16777234	# corresponding Left button in keyboard
KEYCODE_RIGHT = 16777236 # corresponding Right button in keyboard
KEYCODE_OK = 16777220	# corresponding Enter button in keyboard
KEYCODE_QUIT = 6   # Currently QUIT is not supported on the mini board. We are planning to overload it on long press of the button BACK


# FOR PERKINS -> The keycode is the ascii value of the pressed.
# reference: The table on this page - http://en.wikipedia.org/wiki/Braille_ASCII     -> See ASCII glyph corresponding to each braille pattern

# hardware key values
PERKINS_1 = 1<<0;
PERKINS_2 = 1<<1;
PERKINS_3 = 1<<2;
PERKINS_4 = 1<<3;
PERKINS_5 = 1<<4;
PERKINS_6 = 1<<5;
PERKINS_ENTER = 1<<6;
PERKINS_SPACE = 1<<7;
PERKINS_BCK = 1<<8;

braille2ascii = dict()
ascii2braille = dict()

class event():
	def __init__(self, dev, keycode, state):
		self.dev = dev; self.keycode = keycode; self.state = state

	def __str__(self):
		"""A perkins event: 
				dev = DEV_PERKINS
				keycode = AN INTEGER which is the ASCII code for the key pressed.
							- what about the rest of the 2 keys?
				state = its always a KEY_PRESS ... since we are not supporting key up and down.

			HOW to get perkins BINARY code from keycode... use ascii2braille[keycode]
		"""

		return devices[self.dev] + " " + str(self.keycode) + " " + states[self.state]
