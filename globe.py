global RBC, PERKINS, LCD, fakeRBC	# Do we really need these statements? Perhps we dont :P
global ttyMap, ports, SIG
global delim	# This shall be use over all communication

delim = '`\n`~`\n'
# ttyMap = '/dev/tty*'  # for Ubuntu
ttyMap = '/dev/tty.*'  # for MAC
#ttyMap = '/dev/ttyU*'  # for RASPBERRY

RBC = 0
PERKINS = 1
LCD = 2

SIG = ['BRAILLE', 'PERKINS', 'LCD']
ports = {RBC:None, PERKINS:None, LCD:None}		# dictionary of port connectors

DEBUG = True
FAKE = False
FAKE = not(FAKE)		# UNCOMMENT to work with real devices


# IGNORE ----
# def ex(fileName):
# 	open(fileName) as f:
# 		code = compile(f.read(), fileName, 'exec')
# 		exec(code)
