# lcd_objects.py

def concat(args):
	cmd = ""
	for arg in args:
		print arg
		cmd += (str(arg) + "`")
	cmd += "~`"
	return cmd


class LcdObj:
	def __init__(self):
		self.CMD = "clr"
		self.params = []
		pass

	def set(self, kwargs):
		for param in self.params:
			if param in kwargs:
				setattr(self, param, kwargs[param])

	def getCmd(self):
		return self.concat([])

	def concat(self, args):
		cmd = str(self.CMD)
		# print cmd
		for arg in args:
			# print arg
			cmd += ("`" + str(arg))
		cmd += "`~`"
		return cmd


class Img(LcdObj):
	def __init__(self, Id, x, y):
		self.CMD = "img"
		self.params = ['Id', 'x', 'y']
		self.Id = Id
		self.x = x
		self.y = y

	def getCmd(self):
		return self.concat([self.Id, self.x, self.y])


class Rect(LcdObj):
	def __init__(self, x, y, w, h, isRound=0, r=5):
		self.CMD = "rect"
		self.params = ["x", "y", "w", "h", "isRound", "r"]
		self.x = x
		self.y = y
		self.w = w
		self.h = h
		self.isRound = isRound
		self.r = r

	def getCmd(self):
		return self.concat([self.x, self.y, self.w, self.h, self.isRound, self.r])	


class Vline(LcdObj):
	def __init__(self, x, y, w, h):
		self.CMD = "vl"
		self.params = ["x", "y", "h"]
		self.x = x
		self.y = y
		self.h = h
		
	def getCmd(self):
		return self.concat([self.x, self.y, self.h])


class Hline(LcdObj):
	def __init__(self, x, y, w):
		self.CMD = "hl"
		self.params = ["x", "y", "w"]
		self.x = x
		self.y = y
		self.w = w
		
	def getCmd(self):
		return self.concat([self.x, self.y, self.w])



class Text(LcdObj):
	def __init__(self, msg, x, y, w, h, fsz):
		self.CMD = "write"
		self.params = ["msg", "x", "y", "w", "h", "fsz"]
		self.msg = msg
		self.x = x
		self.y = y
		self.w = w
		self.h = h
		self.fsz = fsz

	def getCmd(self):
		return self.concat([self.msg, self.x, self.y, self.w, self.h, self.fsz])


class Font(LcdObj):
	def __init__(self, sz):
		self.CMD = "font"
		self.params = ["sz"]
		self.sz = sz

	def getCmd(self):
		return self.concat([self.sz])


class Inv(LcdObj):
	def __init__(self, x, y, w, h):
		self.CMD = "inv"
		self.params = ["x", "y", "w", "h"]
		self.x = x
		self.y = y
		self.w = w
		self.h = h

	def getCmd(self):
		return self.concat([self.x, self.y, self.w, self.h])


class ClrA(LcdObj):
	def __init__(self, x, y, w, h):
		self.CMD = "clrA"
		self.params = ["x", "y", "w", "h"]
		self.x = x
		self.y = y
		self.w = w
		self.h = h

	def getCmd(self):
		return self.concat([self.x, self.y, self.w, self.h])

# add scrollability
class TextBox(Text):
	# higl_row starts from 0. -1 means no highlighting...
	def __init__(self, msg, x, y, w, h, fsz, higl_row):
		Text.__init__(self, msg, x, y , w, h, fsz)
		self.CMD = "write"
		self.params = ["x", "y", "w", "h", "fsz", "higl_row"]
		self.msg = msg
		self.x = x
		self.y = y
		self.w = w
		self.h = h
		self.fsz = fsz
		self.higl_row = higl_row

	def getCmd(self):
		cmd1 = self.concat([self.msg, self.x, self.y, self.w, self.h, self.fsz])
		if (higl_row < 0):		# higher bound check?
			return cmd1

		fontH = 7			# THIS HAS TO BE TAKEN CARE OF... need to add font sz in Text
		y = self.y + fontH*higl_row
		cmd2 = concat(['inv', self.x, y, w, fontH])
		return cmd1 + cmd2


















