# filename: rbc.py
# exposed functions:
#	One timers: start(), end(), 
# 	Usage     : display()

import log, globe
if (globe.FAKE):
	from test import fakeRBC as serial
else:
	import serial

PORT = ""			# This is like /dev/ttyUSBx, where X is an integer
CIN_TIMEOUT = 1
SZ = 16

Conn = None

def begin(fake_handle = None):
	"""Kind of constructor for this module. Core scan """
	global PORT, Conn, CIN_TIMEOUT
	PORT = globe.ports[globe.RBC]
	
	args = {'port': str(PORT), 'baudrate': 9600, 'timeout':CIN_TIMEOUT}
	if (globe.FAKE):
		args['handle'] = fake_handle
	
	try:
		if (globe.FAKE):
			Conn = serial.Serial(**args)
		else:
			Conn = serial.Serial(str(PORT), 9600, timeout=CIN_TIMEOUT)
	except:
		log.pr("RBC Failed to conn, port name: " + str(PORT))
		return False
	return True

def end():
	"""stops rbc conn"""
	global Conn
	Conn.close()

def display(msg):
	global Conn
	log.Error(msg)
	if (len(msg)>SZ):
		msg = msg[0:SZ]
		log.pr('warning: send long msg than RBC display. str = %s' %msg)
	Conn.write('d' + msg + '\n')